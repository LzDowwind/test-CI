#include <stdio.h>
#include <stdlib.h>

static const char	*color[] = {
  "\x1B[31;1m",
  "\x1B[32;1m",
  "\x1B[33;1m",
  "\x1B[34;1m",
  "\x1B[35;1m",
  "\x1B[36;1m",
  "\x1B[37;1m",
};

int		main(int ac, char **av)
{
  FILE		*file;
  size_t	size;
  char		*s;

  (void)ac;
  size = 0;
  s = NULL;
  if ((file = fopen("doc.txt", "r")) == NULL)
    return (EXIT_FAILURE);
  while (getline(&s, &size, file) > 0)
    {
      if (atoi(av[1]) >= 0 && atoi(av[1]) <= 6)
	printf("%s%s\n", color[atoi(av[1])], s);
      else
	printf("%s%s\n", color[0], s);
      free(s);
      s = NULL;
    }
  free(s);
  fclose(file);
  return (EXIT_SUCCESS);
}
